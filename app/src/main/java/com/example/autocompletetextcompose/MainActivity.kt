package com.example.autocompletetextcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.sp
import com.example.autocompletetextcompose.ui.theme.AutoCompleteTextComposeTheme
import com.example.autocompletetextcompose.ui.theme.Purple500

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AutoCompleteTextComposeTheme {
                Surface(color = MaterialTheme.colors.background) {
                    Scaffold(
                        topBar = { TopBarCompose() },
                        backgroundColor = Purple500
                    ) {
                        CountryNavigation()
                    }
                }
            }
        }
    }

    @Composable
    fun TopBarCompose() {
        TopAppBar(
            title = { Text(text = "Country List", fontSize = 20.sp) },
            backgroundColor = Purple500,
            contentColor = Color.White
        )
    }
}

